// menu toggle
$(function () {
    var html = $('html, body'),
        navContainer = $('.nav-container'),
        navToggle = $('.nav-toggle'),
        navDropdownToggle = $('.has-dropdown');
    overlay = $("<div class='overlay'></div> ");
    overlay2 = $("<div class='overlay'></div> ");

    // Nav toggle
    navToggle.on('click', function (e) {
        overlay.toggle();
        var $this = $(this);
        e.preventDefault();
        $this.toggleClass('is-active');
        navContainer.toggleClass('is-visible');
        html.toggleClass('nav-open');
    });


    $("body").prepend(overlay);
    overlay.click(function () {
        navToggle.trigger('click');
        // $(this).toggle();
    })

    $("body").prepend(overlay2);
    overlay2.click(function () {
        $(this).toggle();
    })
    // Nav dropdown toggle
    navDropdownToggle.on('click', function () {
        var $this = $(this);
        $this.toggleClass('is-active').siblings().removeClass('is-active');
        // if(!$(this).children('ul').is(":visible"))
        // {
        //   $(this).children('ul').slideDown();
        // }
        if ($this.children('ul').hasClass('open-nav')) {
            $this.children('ul').removeClass('open-nav');
            $this.children('ul').slideUp(350);
        }
        else {
            $this.parent().parent().find('li .nav-dropdown').removeClass('open-nav');
            $this.parent().parent().find('li .nav-dropdown').slideUp(350);
            $this.children('ul').toggleClass('open-nav');
            $this.children('ul').slideToggle(350);
        }
    });

    // Prevent click events from firing on children of navDropdownToggle
    navDropdownToggle.on('click', '*', function (e) {
        e.stopPropagation();
    });


});

//scroll to top button
// ----------- croll --------------//
(function ($) {
    //Scroll to Top
    function headerStyle() {
        if ($('.header').length) {
            var windowpos = $(window).scrollTop();
            var scrollLink = $('.scroll-top');
            if (windowpos >= 185) {
                scrollLink.addClass('open');
            } else {
                scrollLink.removeClass('open');
            }
        }
    }
    headerStyle();
    // Scroll to Target
    if ($('.scroll-to-target').length) {
        $(".scroll-to-target").on('click', function () {
            var target = $(this).attr('data-target');
            // animate
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 1000);

        });
    }

    $(window).on('scroll', function () {
        headerStyle();
    });


})(window.jQuery);

// end scroll-------------//
// menu-fix ----->
if (window.innerWidth > 320) {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 84) {
            $('.menu_fix').addClass('fixed_bn');
        }
        else {
            $('.menu_fix').removeClass('fixed_bn');
        }
    });
}

//---------- tăng giảm --------------------->
jQuery(document).ready(function () {
    $('.qtyplus').click(function (e) {
        e.preventDefault();
        fieldName = $(this).attr('field');
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        if (!isNaN(currentVal)) {
            $('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            $('input[name=' + fieldName + ']').val(0);
        }
    });
    $(".qtyminus").click(function (e) {
        e.preventDefault();
        fieldName = $(this).attr('field');
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        if (!isNaN(currentVal) && currentVal > 0) {
            $('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            $('input[name=' + fieldName + ']').val(0);
        }
    });
});
//---------- end tăng giảm --------------------->
/* Menu sidebar */
$('.plus-nClick1').click(function (e) {
    e.preventDefault();
    $(this).parents('.level0').toggleClass('opened');
    $(this).parents('.level0').children('ul').slideToggle(200);
});
$('.plus-nClick2').click(function (e) {
    e.preventDefault();
    $(this).parents('.level1').toggleClass('opened');
    $(this).parents('.level1').children('ul').slideToggle(200);
});

//Click event to scroll to top
jQuery(document).on("click", ".back-to-top", function () {
    jQuery(this).removeClass('show');
    jQuery('html, body').animate({
        scrollTop: 0
    }, 800);
});
/* and Menu sidebar */

jQuery('.title_block').click(function () {
    $(this).next().slideToggle('medium');
});
$(document).on("click", ".dropdown-filter", function () {
    if ($(this).parent().attr('aria-expanded') == 'false') {
        $(this).parent().attr('aria-expanded', 'true');
    } else {
        $(this).parent().attr('aria-expanded', 'false');
    }
});


$(function () {
    $(".slider_main").owlCarousel({
        items: 1,
        responsive: {
            1200: { item: 1, },// breakpoint from 1200 up
            992: { items: 1, },
            768: { items: 1, },
            480: { items: 1, },
            0: { items: 1, }
        },
        rewind: false,
        autoplay: false,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000, //slide speed smooth
        dots: false,
        dotsEach: false,
        loop: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left arrow-slider"></i>', '<i class="fa fa-angle-right arrow-slider"></i>'],
        margin: 10,
        animateOut: ['fadeOutUp', 'zoomOut', 'fadeOutLeft'], // default: false
        animateIn: ['fadeInDown', 'zoomIn', 'fadeInLeft'], // default: false
        center: false,
    });
    $(".slider-product-km").owlCarousel({
        items: 5,
        responsive: {
            1200: { item: 5, },// breakpoint from 1200 up
            992: { items: 4, },
            768: { items: 3, },
            480: { items: 2, },
            0: { items: 1, }
        },
        rewind: false,
        autoplay: false,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000, //slide speed smooth
        dots: false,
        dotsEach: false,
        loop: false,
        nav: true,
        navText: ['<i class="icon-left"></i>', '<i class="icon-right"></i>'],
        margin: 40,
        //animateOut: ['fadeOutUp', 'zoomOut', 'fadeOutLeft'], // default: false
        //animateIn: ['fadeInDown', 'zoomIn', 'fadeInLeft'], // default: false
        center: false,
    });
}); 